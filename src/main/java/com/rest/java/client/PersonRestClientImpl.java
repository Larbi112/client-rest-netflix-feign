package com.rest.java.client;

import java.util.List;
import java.util.Map;

import com.rest.java.client.builder.Builder;
import com.rest.java.client.builder.PersonParamBuilder;
import com.rest.java.client.conf.Configuration;
import com.rest.java.entities.Person;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

public class PersonRestClientImpl implements PersonRestClient {

	private PersonRestClient personRestClient;
	private Configuration config;
	
	//private static final String BASE_URL="http://localhost:8082/v1/person";
	
	private String BASE_URL;
	
	public PersonRestClientImpl(Configuration config) {
		this.config=config;
		initBaseUrl();
		personRestClient=Feign.builder()
				 				.encoder(new JacksonEncoder())
				 				.decoder(new JacksonDecoder())
				 				.target(PersonRestClient.class,BASE_URL);
	}

	private void initBaseUrl() {
		BASE_URL=config.getBaseUrl()+config.getVersion()+config.getComplementUrl();
	}
	
	public Person createPerson(Person person) {
		return personRestClient.createPerson(person);
		}

	public Person updatePerson(Person person) {
		return personRestClient.updatePerson(person);
	}

	public void deletePerson(Long id) {
		personRestClient.deletePerson(id);
		
	}

	public List<Person> getAll() {
		return personRestClient.getAll();
	}

	public List<Person> getAllByName(String name) {
		return personRestClient.getAllByName(name);
	}

	public List<Person> getAllByFamilyName(String familyName) {
		return personRestClient.getAllByFamilyName(familyName);
	}


	public List<Person> getPersons(PersonParamBuilder params) {
		return personRestClient.getPersons(params);
	}

}
