package com.rest.java.client;

import java.util.List;
import java.util.Map;

import com.rest.java.client.builder.Builder;
import com.rest.java.client.builder.PersonParamBuilder;
import com.rest.java.entities.Person;

import feign.Headers;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

@Headers("Accept: application/json")
public interface PersonRestClient {
	
	static final String POST="POST ";
	static final String PUT="PUT ";
	static final String DELETE="DELETE ";
	static final String GET="GET ";
	
	static final String CREATE_PERSON=POST + "/create";
	static final String UPDATE_PERSON=PUT + "/update/{id}";
	static final String DELETE_PERSON=DELETE +"/delete/{id}";
	static final String GET_ALL_PERSONS=GET + "/all";
	static final String GET_PERSONS=GET + "/all/params";
	static final String GET_ALL_PERSONS_BY_NAME=GET + "/all/by/name/{name}";
	static final String GET_ALL_PERSONS_BY_FAMILY_NAME=GET + "/all/by/familyName/{familyName}";
	
	
	
	@Headers("Content-Type: application/json")
	@RequestLine(CREATE_PERSON)
	Person createPerson(Person person);
	
	@Headers("Content-Type: application/json")
	@RequestLine(UPDATE_PERSON)
	Person updatePerson (Person person);
	
	@RequestLine(DELETE_PERSON)
	void deletePerson(@Param(value="id") Long id);
	
	@RequestLine(GET_ALL_PERSONS)
	List<Person> getAll();
	
	//on utilise map car on connait pas le nombre de parametres 
	//a passer et lesquels obligatoires et optionnels
	@RequestLine(GET_PERSONS)
	List<Person> getPersons(@QueryMap PersonParamBuilder params);
	
	@RequestLine(GET_ALL_PERSONS_BY_NAME)
	List<Person> getAllByName(String name);
	
	@RequestLine(GET_ALL_PERSONS_BY_FAMILY_NAME)
	List<Person> getAllByFamilyName(String familyName);
	
	
}
