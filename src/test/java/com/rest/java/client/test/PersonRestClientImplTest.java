package com.rest.java.client.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.rest.java.client.PersonRestClient;
import com.rest.java.client.PersonRestClientImpl;
import com.rest.java.client.builder.Builder;
import com.rest.java.client.builder.PersonParamBuilder;
import com.rest.java.client.conf.Configuration;
import com.rest.java.client.conf.ConfigurationImpl;
import com.rest.java.entities.Person;

public class PersonRestClientImplTest {
	
	public PersonRestClient restClient;
	private Configuration conf;
	
	@Before
	public void setUp() {
		conf=new ConfigurationImpl();
		conf.setBaseUrl("http://localhost:8084/");
		conf.setVersion("v1");
		conf.setComplementUrl("/person");
		restClient=new PersonRestClientImpl(conf);
		
	}

	@Test
	public void testGetAllPersons() {
		List<Person> persons=restClient.getAll();
		System.out.println(persons.size());
		assertTrue(persons.size()==11);
	}
	
	@Test
	@Ignore
	public void createPerson() {
		Person person=new Person();
		person.setName("Alain");
		person.setFamilyName("Bouche");
		person.setAdress("Lannepla");
		Person createdPerson=restClient.createPerson(person);
		assertTrue(createdPerson.getIdPerson()!=null && "Alain".equals(createdPerson.getName()));
	}
	
	@Test
	public void testPrams() {
		PersonParamBuilder params=new Builder()
										.param1("aaaa")
										.param2("bbbb")
										.build();
		List<Person> persons=restClient.getPersons(params);
		assertTrue(persons.size() ==11);
	}		

}
